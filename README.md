# Tech Review 3 : Complexité et Recherche Opérationnelle

> Date: 20 Novembre 2020
>
> Auteur: Olivier Sans <olivier.sans@sciam.fr>
>
> URL de la vidéo: [#3 Complexité et Recherche Opérationnelle](https://caitsconsulting.sharepoint.com/:v:/s/SCIAMCommunicationclients/EQxfIlYuTDtOu-MAVwARH0sBNwuecVAx_yQ9h2zPwXZbfQ?e=Mvi1bv)
>
> Support de présentation: [ #3 - 20201119 - Compléxité et Recherche Opérationnelle.pdf ](https://gitlab.com/sciam/tech-review/complexite-et-ro/-/blob/master/%233%20-%2020201119%20-%20Compl%C3%A9xit%C3%A9%20et%20Recherche%20Op%C3%A9rationnelle.pdf) 

Durant cette présentation, vous avez pu découvrir deux exemples de solveurs de Recherche Opérationnelle. 

Nous avons déposé sur ce dépôt les deux exemples de la présentation. Vous trouverez ci-dessous les informations pour 
récupérer les exemples et les exécuter vous-même.

## Prérequis

 Pour exécuter les deux exemples, nous vous recommandons d'installer [Anaconda](https://www.anaconda.com/). La 
 suite de la documentation sur base sur Anaconda. 
   
 Une installation [Python 3.6](https://www.python.org/) est suffisante pour exécuter le exemples mais vous devrez 
 adapter les instructions. A réserver pour les développeurs python avertis...
 
 Un client git est nécessaire pour télécharger les exemples. Vous pouvez installer 
 [Git for Windows](https://gitforwindows.org/) qui permet un usage de git en ligne de commande. 

## Préparation de l'environnement

Ouvrez le prompt Anaconda en tant qu'administrateur car les instructions suivantes contiennent des commandes 
d'installation de modules python. Pour cela, utilisez le menu démarrer de windows: 
 
 ![Lancer le prompt Anaconda](docs/img/conda-promp-admin.png) 
    
 Récupérez le dépôt git avec les commandes suivantes :
 ```cmd
 > git clone https://gitlab.com/sciam/tech-review/complexite-et-ro.git 
 > cd complexite-et-ro
```
 
 Créez un environnement conda pour installer les dépendances python et exécuter le projet.
```
 > conda create -n env_tech_review python=3.6 pandas jupyter
```
 
 Une confirmation peut vous être demandée. Validez la confirmation ("y" ou "o" selon la langue). 
 A titre d'exemple voici la sortie attendue: 
 
  ```cmd
> conda create -n env_tech_review python=3.6
Collecting package metadata (current_repodata.json): done
Solving environment: done

## Package Plan ##

  environment location: C:\ProgramData\Anaconda3\envs\env_tech_review

  added / updated specs:
    - python=3.6


The following NEW packages will be INSTALLED:

  certifi            pkgs/main/noarch::certifi-2020.6.20-pyhd3eb1b0_3
  pip                pkgs/main/win-64::pip-20.2.4-py36haa95532_0
  python             pkgs/main/win-64::python-3.6.12-h5500b2f_2
  setuptools         pkgs/main/win-64::setuptools-50.3.1-py36haa95532_1
  sqlite             pkgs/main/win-64::sqlite-3.33.0-h2a8f88b_0
  vc                 pkgs/main/win-64::vc-14.1-h0510ff6_4
  vs2015_runtime     pkgs/main/win-64::vs2015_runtime-14.16.27012-hf0eaf9b_3
  wheel              pkgs/main/noarch::wheel-0.35.1-pyhd3eb1b0_0
  wincertstore       pkgs/main/win-64::wincertstore-0.2-py36h7fe50ca_0
  zlib               pkgs/main/win-64::zlib-1.2.11-h62dcd97_4


Proceed ([y]/n)? y

Preparing transaction: done
Verifying transaction: done
Executing transaction: done
#
# To activate this environment, use
#
#     $ conda activate env_tech_review
#
# To deactivate an active environment, use
#
#     $ conda deactivate
```

 Activez l'environnement que vous venez de créer avec les modules requis:
 ```cmd
> conda activate env_tech_review
```
## Solveur Cplex
Installer la librairie [Cplex](https://pypi.org/project/cplex/):
 ```cmd
> conda install -c ibmdecisionoptimization cplex
```
Puis lancer l'exemple :
 ```cmd
> jupyter notebook cplex_example.ipynb
```

Normalement votre navigateur s'ouvre sur le notebook. Si ce n'est pas le cas vous pouvez vous rendre sur
http://localhost:8888/notebooks/cplex_example.ipynb

Dans le notebook, exécutez les différentes cellules à l'aide du bouton de lecture "Exécuter".

 ![Lancer le prompt Anaconda](docs/img/notebook-jupyter.png) 


## Solveur AbsCon
Installer la librairie [Abscon](https://github.com/xcsp3team/pycsp3):
 ```cmd
> pip install pycsp3
```
Si vous souhaitez obtenir les modèles proposés par le [créateur](http://www.cril.fr/~lecoutre) du solveur Abscon, il 
vous suffit de lancer la commande:
 ```cmd
> python -m pycsp3
```
qui importe dans votre dossier courant le dossier `problems` contenant plus de 100 exemples de modélisations. Pour 
exécuter l'exemple de la présentation :
 ```cmd
> python k_color.py -solver=[abscon]
```

_**Remarque :** Si vous utilisez le prompt anaconda PowerShell il vous faudra échapper les crochets de la commande. 
`-solver="[abscon]"`._

Voici un aperçu de la solution au problème:

```cmd
> python k_color.py -solver=[abscon]
  PyCSP3 (Python:3.6.12, Path:C:\ProgramData\Anaconda3\envs\env_tech_review\lib\site-packages\pycsp3\compiler.py)

  * Generating the file k_color.xml completed in ←[92m    0.07←[0m seconds.


  * Solving by AbsCon in progress ...
    with command:  java -cp C:\ProgramData\Anaconda3\envs\env_tech_review\lib\site-packages\pycsp3\solvers\abscon\AbsCon-20-09.jar AbsCon k_color.xml
  * Solved by AbsCon in ←[92m    1.14←[0m seconds

  NB: use the solver option v, as in -solver=[choco,v] or -solver=[abscon,v] to see directly the output of the solver.

<instantiation id="sol1" type="solution">
  <list> France Espagne Portugal Suisse Luxembourg Belgique Italie Pays_Bas Allemagne Autriche R_Tcheque Pologne Slovaquie Hongrie Lituanie Croatie Bosnie Serbie </list>
  <values> 1 2 1 2 2 3 3 1 4 1 2 1 3 2 4 1 2 3 </values>
</instantiation>
```
