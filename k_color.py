from pycsp3 import *

France = Var(range(1,5))
Espagne = Var(range(1,5))
Portugal = Var(range(1,5))
Suisse = Var(range(1,5))
Luxembourg = Var(range(1,5))
Belgique = Var(range(1,5))
Italie = Var(range(1,5))
Pays_Bas = Var(range(1,5))
Allemagne = Var(range(1,5))
Autriche = Var(range(1,5))
R_Tcheque = Var(range(1,5))
Pologne = Var(range(1,5))
Slovaquie = Var(range(1,5))
Hongrie = Var(range(1,5))
Lituanie = Var(range(1,5))
Croatie = Var(range(1,5))
Bosnie = Var(range(1,5))
Serbie = Var(range(1,5))

satisfy(
    France != Espagne,
    France != Suisse,
    France != Luxembourg,
    France != Belgique,
    France != Italie,
    Espagne != Portugal,
    Suisse != Allemagne,
    Suisse != Autriche,
    Suisse != Italie,
    Luxembourg != Belgique,
    Luxembourg != Allemagne,
    Belgique != Pays_Bas,
    Belgique != Allemagne,
    Italie != Autriche,
    Italie != Lituanie,
    Pays_Bas != Allemagne,
    Allemagne != Pologne,
    Allemagne != R_Tcheque,
    Allemagne != Autriche,
    Autriche != R_Tcheque,
    Autriche != Slovaquie,
    Autriche != Hongrie,
    Autriche != Lituanie,
    R_Tcheque != Pologne,
    R_Tcheque != Slovaquie,
    Pologne != Slovaquie,
    Slovaquie != Hongrie,
    Hongrie != Serbie,
    Hongrie != Croatie,
    Hongrie != Lituanie,
    Lituanie !=Croatie,
    Croatie != Serbie,
    Croatie != Bosnie,
    Bosnie != Serbie
)